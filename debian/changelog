gmpc (11.8.16-19) unstable; urgency=medium

  * Set packaging branch to debian/latest
  * Standards-Version: 4.5.1 (no changes required)
  * Update watch file format version to 4

 -- Simon McVittie <smcv@debian.org>  Sat, 26 Dec 2020 14:47:48 +0000

gmpc (11.8.16-18) unstable; urgency=medium

  [ Simon McVittie ]
  * Switch from libappindicator to ayatana-appindicator.
    Thanks to Kentaro Hayashi (Closes: #956768)
  * Remove unused libcurl build-dependency.
    libsoup is now used instead.
  * Drop Depends on gmpc by gmpc-dev.
    gmpc-dev only contains header files, and there is no shared library;
    the gmpc executable is unnecessary for building plugins.
    For Policy compliance, this requires migrating its /usr/share/doc
    directory from symlink to directory.

  [ Helmut Grohne ]
  * Mark gmpc-dev Multi-Arch: same
  * Mark gmpc-data Multi-Arch: foreign (Closes: #893034)

 -- Simon McVittie <smcv@debian.org>  Wed, 19 Aug 2020 09:26:08 +0100

gmpc (11.8.16-17) unstable; urgency=medium

  * d/p/Port-from-obsolete-gnome-doc-utils-to-yelp-tools.patch,
    d/control:
    Switch from gnome-doc-utils to yelp-tools (Closes: #829853, #947528)
  * d/rules: Remove remnants of gmpc-dbg
  * Only build help if we are building gmpc-data
  * Remove unused build-dependency on libglade.
    gmpc has used GtkBuilder instead since 11.8.16.
  * d/p/Don-t-install-remote-man-pages-if-remote-is-disabled.patch:
    Don't install man pages for executables that we disabled
  * Move to debhelper-compat 12
  * Standards-Version: 4.5.0 (no changes required)
  * d/changelog: Trim trailing whitespace

 -- Simon McVittie <smcv@debian.org>  Mon, 10 Feb 2020 15:22:06 +0000

gmpc (11.8.16-16) unstable; urgency=medium

  * d/salsa-ci.yml: Request standard CI on salsa.debian.org
  * d/rules: Remove migration path from gmpc-dbg, which was not in buster
  * Standards-Version: 4.4.1 (no changes required)
  * Disable libunique support (Closes: #885810).
    This removes the gmpc-remote and gmpc-remote-stream programs,
    until/unless someone converts them to use GApplication or do similar
    IPC directly.
  * d/p/Fix-build-with-Vala-0.44.patch,
    d/p/Adjust-ownership-of-GtkRadioButton-groups.patch:
    Fix FTBFS with Vala 0.46
  * d/source/local-options: Remove.
    This breaks the ability to upload with dgit, and is no longer needed
    since dpkg 1.16.1, which automatically unapplies patches after build
    if it applied them before the build.
  * Remove /.gitignore.
    This is a modification to the upstream source code that is not
    reflected in d/patches, which is problematic when using dgit.

 -- Simon McVittie <smcv@debian.org>  Sat, 02 Nov 2019 12:17:53 +0000

gmpc (11.8.16-15) unstable; urgency=medium

  * d/p/Fix-build-failure-with-Vala-0.38.patch:
    Fix FTBFS with Vala 0.38 by declaring a delegate type as static
    (Closes: #921782)
  * Standards-Version: 4.3.0 (no changes required)
  * Set Rules-Requires-Root to no
  * Don't install upstream ChangeLog, which wasn't updated since 2008

 -- Simon McVittie <smcv@debian.org>  Mon, 11 Feb 2019 07:52:02 +0000

gmpc (11.8.16-14) unstable; urgency=medium

  * Switch Vcs-* to salsa.debian.org
  * d/gbp.conf: Add
  * Standards-Version: 4.1.4 (no changes required)
  * Use DEP-14 branch names debian/master, upstream/latest

 -- Simon McVittie <smcv@debian.org>  Sun, 29 Apr 2018 23:25:37 +0100

gmpc (11.8.16-13) unstable; urgency=medium

  * Drop -dbg package and migrate to automatic -dbgsym package
  * Remove unused variables from d/rules
  * Use debhelper compat level 10
    - Do not explicitly depend on dh-autoreconf, which is now the default
  * Standards-Version: 4.1.3
    - Use preferred https URL for d/copyright Format
    - Stop using Priority: extra
  * Use dh_missing to fail the build on missing files
  * Remove build-dependency on libsexy-dev, no longer needed since 11.8.16
  * Use https for Homepage

 -- Simon McVittie <smcv@debian.org>  Sun, 04 Feb 2018 12:06:55 +0100

gmpc (11.8.16-12) unstable; urgency=medium

  [ Jeremy Bicha ]
  * Add generated files to POTFILES.skip
  * Fix build by not trying to install pixmaps

  [ Antoine Beaupré ]
  * merge in patches from Ubuntu (Closes: #864916)

 -- Antoine Beaupré <anarcat@debian.org>  Sat, 29 Jul 2017 17:43:17 -0400

gmpc (11.8.16-11ubuntu1) artful; urgency=medium

  [ Simon McVittie ]
  * Add myself to Uploaders
  * Use cgit for Vcs-Browser
  * Remove old XPM icon, which was used by the menu file before 11.8.16-10

  [ Jeremy Bicha ]
  * Fix build with vala 0.36

 -- Jeremy Bicha <jbicha@ubuntu.com>  Fri, 16 Jun 2017 23:30:18 -0400

gmpc (11.8.16-10) unstable; urgency=medium

  [ Etienne Millon ]
  * Fix FTBFS when built with dpkg-buildpackage -A (Closes: #806034)
  * Add a short license name for gettext license.
  * Use secure protocols for VCS URIs.
  * Bump Standards-Version to 3.9.8 (no changes).
  * Remove menu file.
  * Enable hardening.

  [ Antoine Beaupré ]
  * move maintainership to MPD team, keeping me and etienne as uploaders
  * follow upstream homepage change
  * mention other possible git repos in watch file

 -- Antoine Beaupré <anarcat@debian.org>  Sat, 13 Aug 2016 12:37:32 -0400

gmpc (11.8.16-9) unstable; urgency=medium

  * Fix building with a recent vala (Closes: #739030)
  * Improve package descriptions:
    - don't mention irrelevant details (Closes: #756303)
    - fix capitalization of GNOME
  * Bump Standards-Version to 3.9.5 (no changes).
  * Add libappindicator support (Closes: #736261)
  * Update Vcs-Browser URL.

 -- Etienne Millon <me@emillon.org>  Wed, 27 Aug 2014 14:21:30 +0200

gmpc (11.8.16-8) unstable; urgency=low

  * Add a Breaks clause to force gmpc-plugins to be multiarch-aware.
    (Closes: #725127)
  * Remove outdated README.source.

 -- Etienne Millon <me@emillon.org>  Thu, 10 Oct 2013 11:34:58 +0200

gmpc (11.8.16-7) unstable; urgency=low

  * Fix FTBFS. (Closes: #710623)
    - Force vala version to 0.16.
    - Pass -lm to linker.
  * Bump Standards-Version to 3.9.4 (no changes).
  * Remove version bounds on libmpd-dev (satisfied in oldstable).
  * Set dh compatibility to 9 to enable hardening & multiarch dirs.
  * Disable discogs lyrics provider (Closes: #705164)
  * Disable htbackdrops plugin (Closes: #720918)
  * Update my email address.

 -- Etienne Millon <me@emillon.org>  Sat, 31 Aug 2013 10:43:34 +0200

gmpc (11.8.16-6) unstable; urgency=low

  * Fix catalan menu entry (Closes: #694547)

 -- Etienne Millon <etienne.millon@gmail.com>  Thu, 07 Mar 2013 18:36:17 +0100

gmpc (11.8.16-5) unstable; urgency=low

  * Update translations :
    - [ca] add Catalan translation
    - [ru] fix translation for "volume" (Closes: #687999)
    - [fr], [oc] import from Launchpad

 -- Etienne Millon <etienne.millon@gmail.com>  Fri, 12 Oct 2012 15:48:20 +0200

gmpc (11.8.16-4) unstable; urgency=low

  * Disable LyrDB.com lyrics provider, which segfaults. (Closes: #650359)
  * debian/copyright:
    - Convert to DEP-5.
    - Update copyright information.

 -- Etienne Millon <etienne.millon@gmail.com>  Fri, 17 Aug 2012 22:59:10 +0200

gmpc (11.8.16-3) unstable; urgency=low

  * New patches :
      - Fix timeout when connecting to a slow mpd.
        Thanks to olhotak. (Closes: #656290)
      - Fix FTBFS with valac 0.16 (Closes: #673592)
  * Bump Standards-Version to 3.9.3 (no changes).

 -- Etienne Millon <etienne.millon@gmail.com>  Thu, 24 May 2012 16:29:47 +0200

gmpc (11.8.16-2) unstable; urgency=low

  * Install user manual (Closes: #647701)
  * New patches :
      - Change volume slider sensitivity (Closes: #646803)
      - Bring back images for transport buttons (Closes: #646822)
      - Fix build with valac 0.14 (Thanks to Michael Biebl, Closes: #650219)

 -- Etienne Millon <etienne.millon@gmail.com>  Sun, 18 Dec 2011 16:09:43 +0100

gmpc (11.8.16-1) unstable; urgency=low

  * New upstream version (Closes: #640311)
    - new versioning scheme YY.MM.DD, compatible with the former
  * Remove asterisks in debian/NEWS

 -- Etienne Millon <etienne.millon@gmail.com>  Mon, 05 Sep 2011 13:17:20 +0200

gmpc (0.20.0-2) unstable; urgency=low

  * New maintainer (Closes: #615245)
  * Add a manpage for gmpc-remote-stream(1)
  * Split architecture-independent data into gmpc-data
  * Packaging changes:
      - Switch to 3.0 (quilt) source format
      - Add watchfile
      - Bum Standards-Version to 3.9.2 (no changes)
      - Remove build-dep on quilt
  * Build changes:
      - Explicitly add libraries to fix FTBFS with new toolchain
        (Closes: #554541, thanks to Mahyuddin Susanto)
      - Add -Wl,--as-needed to LDFLAGS
      - Code is now generated from vala at build time
  * debian/control:
      - Replace "Conflicts: gmpc-plugins" with "Breaks:"
      - Better short description for gmpc-dbg
      - Longer extended description for gmpc-dev
  * patches:
      - Fix several typos in log messages
      - Fix a wrong TH line in gmpc-remote(1)

 -- Etienne Millon <etienne.millon@gmail.com>  Tue, 21 Jun 2011 10:06:43 +0200

gmpc (0.20.0-1) unstable; urgency=low

  * New upstream release
  * Remove man-fix-whatis.patch now upstream
  * Added build-dep to valac
  * Bump standards-version (no change)
  * Remove config.log at clean time

 -- Arnaud Cornet <acornet@debian.org>  Sat, 27 Mar 2010 15:04:05 +0000

gmpc (0.19.1-1) unstable; urgency=low

  * New upstream release
  * Bump Standard-Version (no change needed)
  * Tighter debhelper dependency
  * Remove old debian/gmpc.1
  * Fix whatis entries in doc/gmpc{,-remote}.1
  * Add debian/README.source

 -- Arnaud Cornet <acornet@debian.org>  Sat, 24 Oct 2009 00:03:36 +0100

gmpc (0.19.0-1) unstable; urgency=low

  * New upstream release.

 -- Arnaud Cornet <acornet@debian.org>  Mon, 21 Sep 2009 12:16:05 +0200

gmpc (0.18.98-1) unstable; urgency=low

  * New Upstream Version.
  * Ship upstream man pages.
  * debian/rules, use dh7 features and drop cdbs.
  * Ship gmpc-remote.
  * Add conflict on pre 0.18.96 plugins.
  * Drop password-dialog.patch, fixed upstream.
  * Add libsqlite3-dev build depend.
  * Remove README and TODO, useless content.

 -- Arnaud Cornet <acornet@debian.org>  Thu, 13 Aug 2009 09:32:46 +0200

gmpc (0.18.0-3) unstable; urgency=low

  * Add build-depend on libsexy-dev.
  * Fix typo in debian control (packages->package).
  * Add password-dialog.patch to fix disappearing password dialog when
    clicking on 'Save Password' (Closes: #531250). Also add quilt
    build-dependancy.
  * Move gmpc-dbg to debug section.

 -- Arnaud Cornet <acornet@debian.org>  Sun, 31 May 2009 15:18:45 +0200

gmpc (0.18.0-2) unstable; urgency=low

  * Depend on debhelper 7. Bump compat level to 7
  * Add missing build-depend on libsoup.
  * Set myself as maintainer (acked by Decklin Foster).
  * Use Homepage field, update homepage url.
  * Suggest gmpc-plugins.
  * Add gmpc-dbg package.
  * Build in ++build-dir.

 -- Arnaud Cornet <acornet@debian.org>  Sun, 22 Mar 2009 13:12:07 +0100

gmpc (0.18.0-1) unstable; urgency=low

  * New upstream release
  * Bump libmpd deps and Standards-Version

 -- Decklin Foster <decklin@red-bean.com>  Thu, 12 Mar 2009 11:53:16 -0400

gmpc (0.17.0-2) unstable; urgency=low

  * Update build-deps (Closes: #510686)

 -- Decklin Foster <decklin@red-bean.com>  Sun, 04 Jan 2009 11:31:35 -0500

gmpc (0.17.0-1) unstable; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Fri, 02 Jan 2009 23:29:48 -0500

gmpc (0.16.95-1) unstable; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Tue, 02 Dec 2008 20:04:29 -0500

gmpc (0.16.1-2) unstable; urgency=low

  * Clean up a large amount of autocrap and duplicate POs in the diff.
  * Remove quilt (we have no patches).
  * Policy 3.8.0.

 -- Decklin Foster <decklin@red-bean.com>  Tue, 07 Oct 2008 13:22:27 -0400

gmpc (0.16.1-1) unstable; urgency=low

  * New upstream release
    - configure.ac updated for Swedish translation
  * Remove LIB_PATH patch, which was not ever actually applied (NMU did not
    add it to patches/series). The fix is no longer needed; plugins are
    now searched in PACKAGE_LIB_DIR, which can be set without #define
    hacks. (Closes: #459405)
  * Add gob2 to build-deps.

 -- Decklin Foster <decklin@red-bean.com>  Sun, 05 Oct 2008 00:34:16 -0400

gmpc (0.15.5.0-2.2) unstable; urgency=high

  * Non-maintainer upload.
  * Use magic touch in debian/rules to prevent auto* exceution at build time
    (The debian diff.gz. touches configure and configure.ac) Fixes FTBFS.
    Closes: #494241
  * remove Makefile and config.log on clean to prevent patch inflation.

 -- Andreas Metzler <ametzler@debian.org>  Mon, 25 Aug 2008 19:52:24 +0200

gmpc (0.15.5.0-2.1) unstable; urgency=low

  * NMU (package is in LowThresholdNMU list)
  * Add patch to src/main.c to search for plugins in LIB_PATH; set LIB_PATH
    in debian/rules (closes: #459405)
  * debian/menu: fix section
  * debian/control:
      - fix build-dep on libmpd-dev
      - Source-Version -> binary:Version

 -- Leo Costela <costela@debian.org>  Thu, 10 Jul 2008 17:51:09 +0200

gmpc (0.15.5.0-2) unstable; urgency=low

  * Fix unsynchronized -dev deps.

 -- Decklin Foster <decklin@red-bean.com>  Sun, 30 Dec 2007 12:31:20 -0500

gmpc (0.15.5.0-1) unstable; urgency=low

  * New upstream release
    - Obsoletes egg-tray-icon patch
    - Depend on libmpd >= 0.15.0 (sigh)
  * Update URL in debian/copyright (Closes: #458362)

 -- Decklin Foster <decklin@red-bean.com>  Sun, 30 Dec 2007 12:12:03 -0500

gmpc (0.15.1-3) unstable; urgency=low

  * The "And when I get thirsty from drinking the ocean" release.
  * Make gmpc-dev Depends match.

 -- Decklin Foster <decklin@red-bean.com>  Wed, 11 Jul 2007 12:57:39 -0400

gmpc (0.15.1-2) unstable; urgency=low

  * The "This makes me wanna spin round in the yard" release.
  * Use libcurl3 instead of obsolete libcurl4 (Closes: #432578)

 -- Decklin Foster <decklin@red-bean.com>  Tue, 10 Jul 2007 23:16:11 -0400

gmpc (0.15.1-1) unstable; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Fri, 06 Jul 2007 11:20:46 -0400

gmpc (0.14.0-5) unstable; urgency=low

  * Use libcurl4 instead of obsolete libcurl3 (Closes: #423915)

 -- Decklin Foster <decklin@red-bean.com>  Mon, 21 May 2007 13:20:04 -0400

gmpc (0.14.0-4) unstable; urgency=low

  * Add quilt build-dep. (Closes: #417661)

 -- Decklin Foster <decklin@red-bean.com>  Wed, 04 Apr 2007 00:32:09 -0400

gmpc (0.14.0-3) unstable; urgency=low

  * Synchronize gmpc-dev Depends: on libmpd with build-deps.
  * Use fix from gajim for transparency in gtktrayicon.h (Closes: #295492)

 -- Decklin Foster <decklin@red-bean.com>  Tue, 03 Apr 2007 19:34:09 -0400

gmpc (0.14.0-2) unstable; urgency=low

  * Update configure.ac for Swedish translation.
    - clean up autom4te.cache since this will dirty it.

 -- Decklin Foster <decklin@red-bean.com>  Tue, 27 Mar 2007 23:30:43 -0400

gmpc (0.14.0-1) unstable; urgency=low

  * New upstream release
    - Fixes HTML error in French translation (Closes: #397927)
  * Include Swedish translation (Closes: #379115)
  * Bump libmpd build-dep to >= 0.13.0.

 -- Decklin Foster <decklin@red-bean.com>  Tue, 27 Mar 2007 20:56:42 -0400

gmpc (0.13.0-2) unstable; urgency=low

  * Adopting package.

 -- Decklin Foster <decklin@red-bean.com>  Sat, 27 May 2006 20:05:46 -0400

gmpc (0.13.0-1) unstable; urgency=low

  * New upstream version, see /usr/share/doc/gmpc/NEWS.Debian.gz
    Closes: #352674
  * Add new gmpc-dev package for compiling plugins.
  * Upgrade Standards-Version.

 -- Eric Wong <eric@petta-tech.com>  Sat, 11 Mar 2006 19:55:39 -0800

gmpc (0.12.0-1) unstable; urgency=low

  * New upstream version.
  * French translation works.  Closes: #312678
  * Option to start in tray.  Closes: #319884
  * Clicking on the tray icon removes it from the window list.
    Closes: #295493
  * Provides mpd-client.  Closes: #304329

 -- Eric Wong <eric@petta-tech.com>  Sat,  8 Oct 2005 19:30:14 -0700

gmpc (0.11.2-2) unstable; urgency=low

  * removed useless NEWS.gz entry
  * XPM icon added to menu entry

 -- Eric Wong <eric@petta-tech.com>  Sat, 23 Oct 2004 14:25:10 -0700

gmpc (0.11.2-1) unstable; urgency=low

  * new upstream version

 -- Eric Wong <eric@petta-tech.com>  Tue, 19 Oct 2004 21:58:40 -0700

gmpc (0.11.1-2) unstable; urgency=low

  * updated copyright file
  * added watch file to more easily track upstream

 -- Eric Wong <eric@petta-tech.com>  Wed, 25 Aug 2004 03:26:39 -0700

gmpc (0.11.1-1) unstable; urgency=low

  * new upstream
  * improved manpage

 -- Eric Wong <eric@petta-tech.com>  Sun, 18 Jul 2004 19:25:25 -0700

gmpc (0.10.3-1) unstable; urgency=low

  * new upstream

 -- Eric Wong <eric@petta-tech.com>  Fri,  4 Jun 2004 10:09:07 -0700

gmpc (0.10.2-3) unstable; urgency=low

  * added menu

 -- Eric Wong <eric@petta-tech.com>  Fri, 14 May 2004 23:29:05 -0700

gmpc (0.10.2-2) unstable; urgency=low

  * update debhelper dependency

 -- Eric Wong <eric@petta-tech.com>  Wed, 28 Apr 2004 01:24:19 -0700

gmpc (0.10.2-1) unstable; urgency=low

  * new upstream

 -- Eric Wong <eric@petta-tech.com>  Sun, 18 Apr 2004 23:39:45 -0700

gmpc (0.10.1-1) unstable; urgency=low

  * new upstream

 -- Eric Wong <eric@petta-tech.com>  Thu, 25 Mar 2004 21:09:28 -0800

gmpc (0.10.0-1) unstable; urgency=low

  * initial Debianization

 -- Eric Wong <eric@petta-tech.com>  Mon, 15 Mar 2004 00:20:37 -0800
